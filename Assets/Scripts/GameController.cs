﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    [SerializeField]
    private Text RemainingCardsText;
    [SerializeField]
    private Text SetsFoundText;

    public List<Button> cardSlots = new List<Button>();
    public List<int> deck = new List<int>();
    public int[] inPlay = { 0, 0, 0, 0, 0, 0, 0 };
    public List<int> selected = new List<int>();
    public Sprite[] cardFaces;
    public List<Sprite> cardFacesInDeck = new List<Sprite>();

    private int setsFound = 0;
    private int cardClicked;

    private int XORofSet;
    private int cardval;

    // Load the card images as sprites
    // TODO: make this not load all 256 cards when only 63 are needed
    void Awake() {
        cardFaces = Resources.LoadAll<Sprite>("Sprites/");
        Screen.SetResolution(720, 1280, false);
    }

    // Run on startup, after Awake()
    void Start() {
        GetCardSlots();
        AddListeners();
        AddCards();
        Shuffle(cardFacesInDeck);
        DealCards();
        SetsFoundText.text = "Sets found: " + setsFound.ToString();
    }

    // Create an array of the Button objects that hold cards in deck
	void GetCardSlots() {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("CardSlot");

        for (int i=0; i<objects.Length; i++) {
            cardSlots.Add(objects[i].GetComponent<Button>());
            ColorBlock colors = cardSlots[i].colors;
            colors.highlightedColor = Color.white;
            cardSlots[i].colors = colors;
        }
    }

    // Draw cards in card slots
    void AddCards() {
        for (int i=1; i<64; i++) {
            cardFacesInDeck.Add(cardFaces[i]);
        }
    }

    // Deal cards into empty card slots
    // set value of inPlay[n] to integer value of card
    // If no card is available, disable button
    void DealCards() {
        foreach(Button cardSlot in cardSlots) {
            int index = int.Parse(cardSlot.name);
//            Debug.Log("The index I'm trying to access is " + index);
            if (inPlay[index] == 0) {
                if (cardFacesInDeck.Count > 0) { 
                    Sprite temp = cardFacesInDeck[0];
                    inPlay[index] = int.Parse(temp.name);
                    cardFacesInDeck.Remove(temp);
                    cardSlot.image.sprite = temp;
                    // Make opaque
                    Color color = cardSlot.image.color;
                    color.a = 1.0f;
                    cardSlot.image.color = color;
                } else {
                    // Make invisible
                    Color color = cardSlot.image.color;
                    color.a = 0.0f;
                    cardSlot.image.color = color;
                    cardSlot.interactable = false;
                }
            }
        }
        int cardsRemaining = cardFacesInDeck.Count;
        RemainingCardsText.text = "Cards remaining: " + cardFacesInDeck.Count.ToString();

    }

    // Assign functionality to buttons generated after initialization (i.e. card slots)
    void AddListeners() {
        foreach(Button cardSlot in cardSlots) {
            cardSlot.onClick.AddListener(() => clickCard());
        }
    }

    // Checks if the selected values are a valid set
    bool isSet(List<int> cardsSelected) {
        if (cardsSelected.Count < 3) {
            return false;
        } else {
            XORofSet = 0;
            foreach (int card in cardsSelected) {
                if (card >= 0 && card <= 7) { 
                cardval = inPlay[card];
                }
//                Debug.Log("Set was " + XORofSet + ", cardval is " + cardval + ".");
                XORofSet = XORofSet ^ cardval;
            }
            if (XORofSet == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    // Taken from Wikipedia article on Fisher-Yates
    public static void Shuffle(List<Sprite> array) {
        System.Random rng = new System.Random();   // i.e., java.util.Random.
        int n = array.Count;        // The number of items left to shuffle (loop invariant).
        while (n > 1) {
            int k = rng.Next(n);     // 0 <= k < n.
            n--;                     // n is now the last pertinent index;
            Sprite temp = array[n];     // swap array[n] with array[k] (does nothing if k == n).
            array[n] = array[k];
            array[k] = temp;
        }
    }

    public bool isWin() {
        if (cardFacesInDeck.Count > 0) {
            return false;
        } else {
            foreach (Button cardSlot in cardSlots) {
                int index = int.Parse(cardSlot.name);
                if (inPlay[index] != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    // Run when a card in play is clicked:
    // Toggle if it is selected; check for a set
    public void clickCard() {
        // Get the index of the clicked card
        string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;
        cardClicked = int.Parse(name);
        if (selected.Contains(cardClicked)) {
            selected.Remove(cardClicked);
            // Make opaque
            Color color = cardSlots[cardClicked].GetComponent<Image>().color;
            color.a = 1.0f;
            cardSlots[cardClicked].GetComponent<Image>().color = color;
        } else {
            selected.Add(cardClicked);
            // Make semi-transparent
            Color color = cardSlots[cardClicked].GetComponent<Image>().color;
            color.a = 0.5f;
            cardSlots[cardClicked].GetComponent<Image>().color = color;
        }
        if(isSet(selected)) {
            Debug.Log("HEY YOU GOT A SET");
            setsFound++;
            SetsFoundText.text = "Sets found: " + setsFound.ToString();
            // Remove those cards
            foreach(int index in selected) {
                inPlay[index] = 0;
            }
            selected = new List<int>();
            DealCards();
            if(isWin()) {
                RemainingCardsText.text = "You win!";
            }
        }
    }
}