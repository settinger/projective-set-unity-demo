﻿using UnityEngine;
using System.Collections;

public class LayoutAssets : MonoBehaviour {

    [SerializeField]
    private Transform playField;
    [SerializeField]
    private GameObject _cardSlot;

    public Vector3 position = new Vector3(0, 0, 0);

    // Use this for initialization
    void Awake() {
        for (int i = 0; i < 7; i++) {
            GameObject cardSlot = Instantiate(_cardSlot);
            cardSlot.name = i.ToString();
            cardSlot.transform.SetParent(playField);
            position = new Vector3(0, 0, 0);
            if (i < 2) { position.y = 300; }
            else if (i > 4) { position.y = -300; }
            else { position.y = 0; }
            if (i == 2) { position.x = -200; }
            else if (i == 4) { position.x = 200; }
            else if (i == 0 || i == 5) { position.x = -100; }
            else if (i == 1 || i == 6) { position.x = 100; }
            else { position.x = 0; }
            position.z = 0;
            cardSlot.transform.localPosition = position;
        }
	}
}
